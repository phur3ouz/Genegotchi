﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditorInternal;

public class CreatureBehaviour : MonoBehaviour
{
	private static List<Creature> creatureList = new List<Creature>();
	private static Creature _creature;

	public static int state;
	public static bool focused;
	public static GameObject[] currentDisplay;

	public static Creature creature
	{
		get
		{
			return _creature;
		}
		set
		{
			_creature = value;
			creatureList.Add(value);
		}
	}

	public static void Switch()
	{
		if (creatureList.Count < 2)
		{
			creature = new Creature();
		}
		else
		{
			int index = creatureList.IndexOf(creature);
			creature = creatureList[(++index) % 2];
		}
	}
}