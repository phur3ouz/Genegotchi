using UnityEngine;
using UnityEngine.UI;

public class CreatureController : CreatureBehaviour
{
	public void SetName(Text creatureName)
	{
		creature.SetName(creatureName.text);
	}
}