﻿using UnityEngine;

namespace Model
{
	public class CreatureModel : CreatureBehaviour {

		void Start () {
			creature = new Creature();

			state = 0;
		}

		void LateUpdate()
		{
			if (focused)
				return;

			if (Input.GetKey(KeyCode.Space) || Input.GetKeyDown(KeyCode.Mouse1))
			{
				Switch();
			}
			if (Input.GetKeyDown(KeyCode.W))
			{
				state = (state + 1) % 2;
			}
			if (Input.GetKeyDown(KeyCode.H))
			{
				creature.SetHealthValue(creature.GetHealthValue() - 5);
			}
			if (Input.GetKeyDown(KeyCode.E))
			{
				creature.SetExperienceValue(creature.GetExperienceValue() + 5);
			}
		}
	}
}
