using UnityEngine;
using System.Collections;

public class GeneticController : MonoBehaviour {
	/*
	 * # Biomes
	 * Hot
	 *	Mountain
	 *	Volcano
	 *	Planetary core
	 *	Desert
	 * Cold
	 *	Water
	 *	Ice
	 *	Outer space
	 *	Cave
	 * Wet
	 *	Water
	 *	Swamp
	 * Dry 
	 *	Desert
	 * Lush
	 *	Forest
	 *	Jungle
	 *	Field
	 *	Plain
	 * 
	 * 
	 * # Creature / Person / Race / Etc
	 * # Body
	 * Weight
	 * Height
	 * Colour
	 * 
	 * Gender
	 * Warm / Cold blooded
	 * Speed
	 * Reaction
	 * Hostility/Friendliness
	 * Languages / Sounds
	 * Magical powers (if any)
	 * Age
	 * Size at birth
	 * Current size
	 * Birthday
	 * Senses / Extra / Missing parts
	 * Shape / Figure
	 * Structure
	 * Ambidextrous / Primary / Interactive body part (Hand / Claw / Tail)
	 * Family type (Dog)
	 * Weaknesses to environment
	 * Strengths to environment (disesases)
	 * Favourite environment
	 * Most hated environment
	 * Favourite Food
	 * Most disliked food
	 * 
	 * Fluids that exist in the body
	 * 
	 * Molecular structure (Solid liquid gas)
	 * Plant or Animal
	 * 
	 */

	/*		Abstract stats
	 * Colour
	 */

	private int lines = 10;

	private void Start () {
		lines *= 2;
		GetComponent<MeshFilter>().mesh = ProceduralMeshGeneration();
	}

	private Mesh ProceduralMeshGeneration() {
		MeshFilter meshFilter = GetComponent<MeshFilter>();
		Mesh mesh = meshFilter.sharedMesh;
		//Mesh mesh = new Mesh();

		mesh.Clear();

		mesh.vertices = CreateVertices();
		mesh.triangles = CreateTriangles();

		mesh.RecalculateNormals();
		mesh.RecalculateBounds();
		mesh.Optimize();

		return mesh;
	}
	

	private Vector3[] CreateVertices() {
		Vector3[] vertices = new Vector3[lines];

		for (int i = 0; i < vertices.Length; i += 2) {
			float a = Random.Range(-5.0f, 5.0f);
			float b = Random.Range(-5.0f, 5.0f);
			vertices[i] = new Vector2(Mathf.Min(a, b), -i / 2.0f);
			vertices[i + 1] = new Vector2(Mathf.Max(a, b), -i / 2.0f);
		}

		return vertices;
	}

	private int[] CreateTriangles() {
		int vertexCount = (lines - 2) * 3 * 2;
		int[] triangles = new int[vertexCount];

		for (int i = 0; i < vertexCount; i += 12) {
			int value = i / 6;

			triangles[i] = value;
			triangles[i + 1] = value + 1;
			triangles[i + 2] = value + 2;

			triangles[i + 3] = value;
			triangles[i + 4] = value + 2;
			triangles[i + 5] = value + 3;

			triangles[i + 6] = value + 2;
			triangles[i + 7] = value + 1;
			triangles[i + 8] = value + 3;

			triangles[i + 9] = value + 0;
			triangles[i + 10] = value + 3;
			triangles[i + 11] = value + 1;
		}

		return triangles;
	}
	
}
