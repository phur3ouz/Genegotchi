﻿//Theoretical maximum genes for any creature of this species

using Extension;

public class Species : Gene {
	//Name
	private string _name;

	private Body _babyBody;
	private Body _adultBody;

	private Attribute speciesAttributes;
	//Capture other creatures / harvest plants for food, etc
	private Diet diet;

	public Species() {
		//float[] stats = geneValues.Absolute();

		_babyBody = new Body(geneValues.Absolute());
		_adultBody = new Body(geneValues.Reverse().Absolute(), MinSize());
		speciesAttributes = Attribute.GenerateAttributes(geneValues.Absolute());
		//attributes = Attribute.GenerateAttributes(geneValues.Absolute(), MaxSize());
	}

	//Name
	public string GetName() {
		return _name ?? "Unnamed";
	}
	public void SetName(string newName) {
		_name = newName;
	}

	//Attribute
	public Attribute GetAttributes() {
		return speciesAttributes;
	}


	public Body GetBabyBody() {
		return _babyBody;
	}

	//BabySize (Default)
	public float MinSize() {
		return _babyBody.GetSize();
	}
	//AdultSize (Default)
	public float MaxSize() {
		return _adultBody.GetSize();
	}
	public float GetSizeDifference() {
		return MaxSize() - MinSize();
	}
	public float GetSizeDifferenceBounds() {
		return GetSizeDifference() / 5.0f;
	}
	public string GetSizeText(string type) {
		type = type.ToUpper();
		if (type.Equals("MIN")) {
			return MinSize().ToString("F2") + "cm";
		} else
		if (type.Equals("MAX")) {
			return MaxSize().ToString("F2") + "cm";
		}

		return "";
	}

	/*
 * Optional
 *	Limbs
 * Sensory features
 *	Sight (eyes)
 *	Hearing (ears)
 *
*/
}
