﻿using System;
using Extension;
using UnityEngine;

public class Creature : Species{

//	//Birth stats
//	private float birthSize;
//	private Color birthColour;
//	private Attribute birthAttributes;
//	//Adult stats
//	private float finalSize;
//	private Color finalColour;
//	private Attribute finalAttributes;
//
//	//Baby
//	private int generation;
//
//	private string name;
//	private System.DateTime dob;
//	private int age;
//	private Gender gender;
//
//	private Body body;

	private Attribute speciesAttributes;
	private Attribute creatureAttributes;

	private float health;
	private float level;
	private float experience;


	public Creature()
	{
		float[] newGeneValues = AlterGeneValuesALittleBit();

		//dob = DateTime.Today; //MURICAN!

		speciesAttributes = base.GetAttributes();
		creatureAttributes = Attribute.GenerateAttributes(newGeneValues.Absolute());

		//Turn to attributes also
		health = 100;
		level = 1;
		experience = 0;

		//_Creature();
	}

	public float GetHealthValue()
	{
		return health;
	}
	public string GetHealthText()
	{
		return "Health: " + GeneralExtension.Float2Dp(health);
	}
	public void SetHealthValue(float newHealth)
	{
		health = newHealth;
	}

	public float GetExperienceValue()
	{
		return experience;
	}
	public string GetExperienceText()
	{
		return "Experience: " + GeneralExtension.Float2Dp(experience);
	}
	public void SetExperienceValue(float newExperience)
	{
		experience = newExperience;
	}

	public string GetLevelText()
	{
		return level.ToString();
	}

//	//Birth stats
//	public void _Creature() {
//		float[] newGeneValues = AlterGeneValuesALittleBit();
//
//		age = 1;
//		//mother
//		//father
//		dob = DateTime.Now;
//		gender = Gender.Male;
//
//		body = new Body(this, newGeneValues);
//
//		speciesAttributes = base.GetAttributes();
//		creatureAttributes = Attribute.GenerateAttributes(newGeneValues.Absolute());
//
//		SetBirthStats();
//	}
//
//
//	private void SetStats() {
//		generation = 1;
//	}
//	private void SetBirthStats() {
//		birthSize = GetSize();
//		birthColour = body.GetColour();
//		birthAttributes = GetAttributes();
//	}
//	private void SetFinalStats() {
//		finalSize = GetSize();
//		finalColour = body.GetColour();
//		finalAttributes = GetAttributes();
//	}
//
//	public void Update() {
//		if (Time.time > Global.time + 0.07f) {
//			Global.time += 0.07f;
//			age++;
//		}
//	}
//
//	//Name
////	public string GetName() {
////		return name ?? "Unnamed";
////	}
////	public void SetName(string newName) {
////		name = newName;
////	}
//
//	//Age
//	public int GetAge() {
//		return age;
//	}
//
//	//DOB
//	public DateTime GetDateOfBirth()
//	{
//		return dob;
//	}
//
//	public string GetDateOfBirthText()
//	{
//		return "Date of Birth: " + GetDateOfBirth().ToString();
//	}
//
//	//Gender
//	public Gender GetGender() {
//		return gender;
//	}
//
//	//Body
//	public float GetSize() {
//		return body.GetSize();
//	}
//	public string GetSizeText() {
//		return GetSize().ToString("F2") + "cm";
//	}
//	public string GetSizeTag() {
//		//Depending on age?
////		float size = 0;
////		if (age < 20)
////			size = MinSize();
////		else
////			size = MaxSize();
//
//		string tag = "";
//		if (Something(MinSize(), 50)) {
//			tag = "Tiny";
//		} else
//		if (Something(MinSize(), 85)) {
//			tag = "Small";
//		} else
//		if (Something(MinSize(), 115)) {
//			tag = "Average";
//		} else
//		if (Something(MinSize(), 130)) {
//			tag = "Big";
//		} else
//		if (Something(MinSize(), 175)) {
//			tag = "Large";
//		} else
//			tag = "Gigantic";
//
//		return tag;
//	}
//	private bool Something(float size, float percentage) {
//		return GetSize() <= (size / 100.0f * percentage);
//	}

	//Attribute
//	public Attribute GetAttributes() {
//		return creatureAttributes;
//	}

	public float GetAttributeValue(string attribute, bool species = false) {
		return species ? creatureAttributes[attribute].GetValue() : speciesAttributes[attribute].GetValue();
	}
	public string GetAttributeText(string attribute) {
		return attribute + " (" + creatureAttributes[attribute].GetValueText() + "/" + speciesAttributes[attribute].GetValueText() + ")";
	}

}
