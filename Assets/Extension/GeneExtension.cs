﻿using System.Linq;
using UnityEngine;

namespace Extension
{
	public static class GeneExtension {

		//Absolute
		public static float[] Absolute(this Gene gene) {
			return gene.geneValues.Absolute();
		}
		public static float[] Absolute(this float[] gene) {
			return gene.Select(x => Mathf.Abs(x)).ToArray();
		}
		//Reverse
		public static float[] Reverse(this Gene gene) {
			return gene.geneValues.Reverse();
		}
		public static float[] Reverse(this float[] gene) {
			return gene.Reverse<float>().ToArray();
		}
		//Take
		public static float[] Take(this Gene gene, int i) {
			return gene.geneValues.Take(i);
		}
		public static float[] Take(this float[] gene, int i) {
			return gene.Take<float>(i).ToArray();
		}
		//Skip
		public static float[] Skip(this Gene gene, int i) {
			return gene.geneValues.Take(i);
		}
		public static float[] Skip(this float[] gene, int i) {
			return gene.Skip<float>(i).ToArray();
		}

	}
}
