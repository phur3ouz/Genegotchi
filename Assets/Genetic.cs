using UnityEngine;
using System.Collections.Generic;
using System.Linq;

class Genetic : CreatureBehaviour {

	Dictionary<string, float> body = new Dictionary<string, float>();
	/*
	 * Weight - float
	 * Height - float
	 * 
	 * Colour - Color
	 * Shape - string
	 * Structure/Form - string
	 * Element - string
	 * 
	 * Gender - int
	 */

	Dictionary<string, object> attackdefense = new Dictionary<string, object>();
	/*
	 * Eating Strategy (C/O/H) 
	 *	C:	Attack method
	 *		Strength (Agility / Teamwork / Scavenger)
	 *		Weakness (Slow / Unprotected area)
	 *	H:	Defense method
	 *		Weakness
	 */



}


//Theoretical maximum genes for any creature of this species


public class Attr {

	private float value;

	public Attr(float attributeValue) {
		value = attributeValue;
	}

	public float GetValue() {
		return value;
	}
	public string GetValueText() {
		return value.ToString("F2");
	}

	//What's a struct?
	//implicit
	//operator


}

public class Attribute : Dictionary<string, Attr>
{

	private Dictionary<string, Attr> attribute;

	public Attribute()
	{
		return;
		attribute = new Dictionary<string, Attr>();

		attribute["Offense"] = null;
		attribute["Defense"] = null;
		attribute["Mobility"] = null;
	}

	public float GetValue(string key) {
		return this[key].GetValue();
	}

	//Static functions
	public static Attribute GenerateAttributes(float[] stats, float size = 30) {
		//Constant value - completely dependent on size. 300 offense and tiny vs 100 offense and massive, 100 gets a much bigger bonus
		float o = stats[7] + (size * size / 30) - size;
		float d = stats[8] + (size * size / 30) - size;
		float m = stats[9] + (30 * 30 / size) - 30;

		//How does it really extend it.?
		return new Attribute {
			{ "Offense", new Attr(o) },
			{ "Defense", new Attr(d) },
			{ "Mobility", new Attr(m) },
		};
	}

}

class Colour : ColourScheme {

	private Color colour;

	public Colour(float[] stats) : base(stats) {
		float min = stats.Min();

		colour = new Color(
			min / stats[GetA()],
			min / stats[GetB()],
			min / stats[GetC()]
		);
	}

	public Color GetColour() {
		return colour;
	}

}

public class ColourScheme {

	private int[] rgb = new int[3];

	public ColourScheme(float[] stats) {
		float f = Mathf.Max(stats[0], stats[1]) % Mathf.Min(stats[0], stats[1]);
		if (float.IsNaN(f) || float.IsInfinity(f)) {
			f = stats[2];
		}
		int asd = Mathf.RoundToInt(f * stats[2]);

		for (int i = 0; i < 3; i++) {
			rgb[i] = Mathf.Abs(asd + i) % 3;
		}

	}

	protected int GetA() {
		return rgb[0];
	}

	protected int GetB() {
		return rgb[1];
	}

	protected int GetC() {
		return rgb[2];
	}

	protected int[] GetColourScheme() {
		return rgb;
	}

}

//Maybe BodyAttr + BodyAttribute


//Miscellaneous




//static class ClassExtension {
//	/* AGE */
//	public static string GetLifeAge(this Creature creature) {
//		return GetLifeAge(creature.GetAge());
//	}
//	private static string GetLifeAge(int age) {
//		string[] ages = new string[4];
//
//		ages[0] = age % 24 + "H";
//		ages[1] = ((age / 24) % 7) > 0 ? ((age / 24) % 7) + "D, " : "";
//		ages[2] = (age / (24 * 7) % 52) > 0 ? (age / (24 * 7) % 52) + "W, " : "";
//		ages[3] = (age / (24 * 7 * 52)) > 0 ? (age / (24 * 7 * 52)) + "Y, " : "";
//
//		return ages[3] + ages[2] + ages[1] + ages[0];
//	}
//	public static string GetLifeStage(this Creature creature) {
//		return GetLifeStage(creature.GetAge());
//	}
//	private static string GetLifeStage(float age) {
//		if (age < (24 * 7))
//			return "Baby";
//		if (age < (24 * 7 * 4))
//			return "Child";
//		if (age < (24 * 7 * 18))
//			return "Adolescent";
//
//		return "Adult";
//	}
//
//	/* GENES */
//
//}


class Personality : Nature{
	//Friendly
	//Playful / Serious
	//Companion
	//Teamwork / Individual
}

class Diet {

}

//Unique creatures - with genes:
//AAAAAA
//BBBBBB
//CCCCCC
//DDDDDD
//......
//ZZZZZZ
//Other weird words

//Change background depending on biome
class Biome {

	/* # Biomes
	* Hot
	*	Mountain	Volcano		Planetary core		Desert
	* Cold
	*	Water		Ice			Outer space			Cave
	* Wet
	*	Water		Swamp
	* Dry 
	*	Desert
	* Lush
	*	Forest		Jungle		Field				Plain
	*/

	public Biome() {

	}

	float totalBiomes = 10;
	//float spawnFormula = ((0 + 17.3f) * 4.2f) % totalBiomes;


}

enum Status {
	Normal,
	//Positive

	//Negative
	Sick
}
