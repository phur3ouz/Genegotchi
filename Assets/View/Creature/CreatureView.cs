﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace View.Creature
{
	public class CreatureView : CreatureBehaviour
	{

		public InputField input;
		public Image image;

		public GameObject[] attributes;
		private Slider[][] attributesSlider;
		private Text[] attributesText;

		public GameObject[] statistics;
		private Slider[] statisticsSlider;
		private Text[] statisticsText;

		private GameObject[] currentDisplay;
		private int prevState;

		private void Start()
		{
			InitializeAttributes();
			InitializeStatistics();

			HideAll();

			currentDisplay = attributes;
			prevState = -1;
		}

		private void Update()
		{
			focused = input.isFocused;
			if (focused)
				return;

			if (state != prevState)
			{
				prevState = state;
				foreach (GameObject go in currentDisplay)
					go.SetActive(false);

				switch (state)
				{
					case 0:
						currentDisplay = attributes;
						break;
					case 1:
						currentDisplay = statistics;
						break;

				}

				foreach (GameObject go in currentDisplay)
					go.SetActive(true);

			}

			input.text = creature.GetName();
			DisplayAttributes();
			DisplayStatistics();

		}

		private void InitializeAttributes() {
			attributesSlider = new Slider[2][];
			attributesSlider[0] = new Slider[attributes.Length];
			attributesSlider[1] = new Slider[attributes.Length];
			attributesText = new Text[attributes.Length];

			for (int i = 0; i < attributes.Length; i++) {
				attributesText[i] = attributes[i].GetComponentInChildren<Text>();
				attributesSlider[0][i] = attributes[i].GetComponentsInChildren<Slider>()[0];
				attributesSlider[1][i] = attributes[i].GetComponentsInChildren<Slider>()[1];
			}
		}

		private void InitializeStatistics()
		{
			statisticsText = new Text[statistics.Length];
			statisticsSlider = new Slider[statistics.Length];

			for (int i = 0; i < statistics.Length; i++)
			{
				statisticsText[i] = statistics[i].GetComponentInChildren<Text>();
				statisticsSlider[i] = statistics[i].GetComponentInChildren<Slider>();
			}
		}

		private void HideAll()
		{
			foreach (GameObject attribute in attributes)
			{
				attribute.SetActive(false);
			}

			foreach (GameObject statistic in statistics)
			{
				statistic.SetActive(false);
			}
		}

		private void DisplayAttributes() {
			if (currentDisplay != attributes)
				return;

			for (int i = 0; i < attributes.Length; i++) {
				attributesText[i].text = creature.GetAttributeText(Global.attributes[i]); //Stat text
				attributesSlider[0][i].value = creature.GetAttributeValue(Global.attributes[i]); //Stat slider: Species
				attributesSlider[1][i].value = creature.GetAttributeValue(Global.attributes[i], true); //Stat slider: Creature
			}
		}

		private void DisplayStatistics() {
			if (currentDisplay != statistics)
				return;

			//Health
			statisticsText[0].text = creature.GetHealthText();
			statisticsSlider[0].value = creature.GetHealthValue();
			//Experience
			statisticsText[1].text = creature.GetExperienceText();
			statisticsSlider[1].value = creature.GetExperienceValue();
		}

/*
			//Creature UI stuff
			public Text geneText;
			public InputField creatureName;

			public Text[] speciesMisc;
			public GameObject[] attributes;
			public Text[] creatureMisc;
			private Slider[][] attributesSlider;
			private Text[] attributesText;

			private void InitializeVariables() {
				attributesSlider = new Slider[2][];
				attributesSlider[0] = new Slider[attributes.Length];
				attributesSlider[1] = new Slider[attributes.Length];
				attributesText = new Text[attributes.Length];

				for (int i = 0; i < attributes.Length - 1; i++) {
					attributesText[i] = attributes[i].GetComponentInChildren<Text>();
					attributesSlider[0][i] = attributes[i].GetComponentsInChildren<Slider>()[0];
					attributesSlider[1][i] = attributes[i].GetComponentsInChildren<Slider>()[1];
				}
			}

			private void DoCreatureUIStuff() {
				geneText.text = "Gene Code: [" + creature.geneName + "]";

				for (int i = 0; i < 3; i++) {
					attributesText[i].text = creature.GetAttributeText(Global.attributes[i]); //Stat text
					attributesSlider[0][i].value = creature.GetAttributeValue(Global.attributes[i]); //Stat slider: Species
					attributesSlider[1][i].value = creature.GetAttributeValue(Global.attributes[i], true); //Stat slider: Creature
				}

				attributes[3].GetComponent<Image>().color = creature.GetBabyBody().GetColour();
			}

			private void DoSpeciesUIStuff() {
				speciesMisc[0].text = "Min Size: " + creature.GetSizeText("min") + "\t" + creature.GetSizeText();
				speciesMisc[1].text = "Max Size: " + creature.GetSizeText("max") + "\t" + creature.GetSizeTag();
			}

			private void Start()
			{
				creature = new Creature();
				InitializeVariables();
				DoCreatureUIStuff();
				DoSpeciesUIStuff();
			}

			private void Update() {
				creature.Update();
				creatureMisc[0].text = "Age: " + creature.GetLifeAge();
				creatureMisc[1].text = "Stage of Life: " + creature.GetLifeStage();

				if (Input.GetKey(KeyCode.Space) || Input.GetKeyDown(KeyCode.Mouse0))
				{
					creature = new Creature();

					Global.time = Time.time;
					DoCreatureUIStuff();
					DoSpeciesUIStuff();
				}

				if (Input.GetKeyDown(KeyCode.Z)) {
					Debug.Log(creature.GetName());
					Debug.Log(creature.GetGender());
				}
			}

		*/

	}
}
