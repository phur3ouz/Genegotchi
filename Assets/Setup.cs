using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;

public class Setup : MonoBehaviour {

	private void Awake() {
		string path = @"C:\Users\Phur3ouZ\Desktop\text.txt";
		if (!File.Exists(path)) {
			WriteToFile(path);
		}
		ReadFromFile(path);

		Destroy(this);
	}

	private string CreateGene(int i) {
		string text = Global.genePool[i].ToString();

		List<int> list = new List<int>();
		for (int j = 0; j < 6; j++) {
			int number = Random.Range(0, 10);
			if (!list.Contains(number)) {
				text += number + Random.Range(-100.0f, 100.0f).ToString("F1") + ",";
				list.Add(number);
			} else {
				j--;
			}
		}
		return text.Substring(0, text.Length - 1);
	}

	private void WriteToFile(string path) {
		FileStream file = File.Create(path);
		StreamWriter writer = new StreamWriter(file);

		try {
			for (int i = 0; i < Global.genePool.Length; i++) {
				writer.WriteLine(CreateGene(i));
			}
		} catch (System.Exception) {
		} finally {
			writer.Close();
			file.Close();
		}
	}

	private void ReadFromFile(string path) {
		FileStream file = File.Open(path, FileMode.Open);
		StreamReader reader = new StreamReader(file);

		try {
			while (!reader.EndOfStream) {
				string line = reader.ReadLine();
				float[] tempStats = new float[10];

				//This is the DNA ('a', 'b', ..)
				char geneCode = line[0];

				//This is the value of the DNA
				string[] values = line.Substring(1).Split(',');

				foreach (string value in values) {
					//Could have checks here.. but I'm lazy
					int statIndex = (int)char.GetNumericValue(value[0]);
					float adjustmentValue = float.Parse(value.Substring(1));

					tempStats[statIndex] = adjustmentValue;
				}

				Global.geneValue.Add(geneCode, tempStats);
			}
		} catch (System.Exception) {
		} finally {
			reader.Close();
			file.Close();
		}

	}
}
