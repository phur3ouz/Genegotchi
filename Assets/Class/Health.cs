using UnityEngine;
using System.Collections;

public class Health {

	private float hp;
	private float hunger;
	private float happiness;

	private Status status;

	public Health() {
		hp = 100.0f;
		hunger = 0.0f;
		happiness = 50.0f;

		status = Status.Normal;
	}


}
