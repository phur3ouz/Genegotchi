﻿using UnityEngine;

public class Gene {

	//_string
	//_value

	//Determine what all the values do

	public readonly string geneName = "";
	public readonly float[] geneValues = new float[10];

	//Generate gene string and associated value
	public Gene() {
		for (int i = 0; i < 6; i++) {
			geneName += Global.genePool[Random.Range(0, Global.genePool.Length)];
		}

		for (int index = 0; index < geneName.Length; index++) {
			char c = geneName[index];

			switch (index) {
				case 2:
					for (int i = 0; i < Global.geneValue[c].Length; i++) {
						geneValues[i] += -Mathf.Abs(Global.geneValue[c][i]);
					}
					break;
				case 5:
					for (int i = 0; i < Global.geneValue[c].Length; i++) {
						geneValues[i] += Mathf.Abs(Global.geneValue[c][i]);
					}
					break;
				default:
					for (int i = 0; i < Global.geneValue[c].Length; i++) {
						geneValues[i] += Global.geneValue[c][i];
					}
					break;
			}
		}
	}

	public float[] AlterGeneValuesALittleBit() {
		float[] newValues = (float[])geneValues.Clone();

		for (int i = 0; i < newValues.Length; i++) {
			newValues[i] *= Random.Range(0.95f, 1.05f);
		}

		return newValues;
	}

}