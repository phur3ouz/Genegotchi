using System.Collections.Generic;
using UnityEngine;

public class Global
{
	public static float time = 0;

	public static readonly float repeatInterval = 4.0f / 8;

	public static readonly string genePool = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz";
	public static Dictionary<char, float[]> geneValue = new Dictionary<char, float[]>();

	public static readonly string[] attributes = { "Offense", "Defense", "Mobility" };
	public static readonly string[] statistics = { "Health", "Experience" };

}
