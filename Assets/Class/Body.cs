﻿using Extension;
using UnityEngine;

public class Body {

	private Colour bodyColour;
	private float bodySize;

	private string composition; //Solid / Liquid / Gas

	private string skinType; //aka. Coat Material. Bare; Furry; Scaly;
	private string shape; //Composite of random points?
	private string element; //Fire / Water / Earth -- biome related?

	//Baby species body
	public Body(float[] stats) {
		bodyColour = new Colour(stats.Take(3));
		bodySize = CalculateSize(stats);
	}
	//Adult species body
	public Body(float[] stats, float babySize) {
		bodySize = CalculateSize(stats, 7) + babySize;
	}
	//Creature body
	public Body(Species species, float[] stats) {
		bodySize = Mathf.Abs(species.MinSize() + Random.Range(-species.GetSizeDifferenceBounds(), species.GetSizeDifferenceBounds()));
		bodyColour = new Colour(stats.Take(3));
	}

	//Recursively get size..
	private float CalculateSize(float[] f, float multiplier = 1) {
		if (f.Length <= 0)
			return 0;

		float total = 0;
		foreach (float value in f) {
			if (Mathf.RoundToInt(value % 2) == 0)
				total += Mathf.Abs(value / f.Length);
			else
				total *= Mathf.Abs(value / Mathf.Pow(f.Length, 2));

			total++; //Gets rid of the zero-ing
		}

		return (total - ((total / 2) - Mathf.Abs(CalculateSize(f.Skip(1)) / 3))) * multiplier;
	}

	public Color GetColour() {
		return bodyColour.GetColour();
	}

	//Size
	public float GetSize() {
		return bodySize;
	}
	public bool IsSmall() {
		return bodySize <= 5;
	}
	public bool IsLarge() {
		return bodySize >= 230;
	}
}

